package com.olenaznak.manager;

import com.olenaznak.consoleworker.ConsoleInput;
import com.olenaznak.goods.FlowerGood;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FlowerpotOrder {


    List<FlowerStock.FlowerItem> order = new ArrayList<>();
    List<FlowerGood> flowerpots = new LinkedList<>();

    public FlowerpotOrder(List<FlowerStock.FlowerItem> order, List<FlowerGood> flowerpots) {
        this.order = order;
        this.flowerpots = flowerpots;
    }

    public FlowerpotOrder() {
    }

    public List<FlowerStock.FlowerItem> getOrder() {
        return order;
    }

    public void setOrder(List<FlowerStock.FlowerItem> order) {
        this.order = order;
    }

    public List<FlowerGood> getFlowerpots() {
        if(flowerpots == null){
            return null;
        }
        sortFlowerpots();
        return flowerpots;
    }

    public void setFlowerpots(List<FlowerGood> flowerpots) {
        this.flowerpots = flowerpots;
    }

    public void orderFlowerpot(){
        ConsoleInput in = new ConsoleInput();
        printMenu();
        System.out.println("Choose flowerpot");
        boolean cont = true;
        while(cont) {
            int i = in.getInt();
            switch (i) {
                case 1:
                    order.add(FlowerStock.FlowerItem.CACTUS);
                    break;
                case 2:
                    order.add(FlowerStock.FlowerItem.ORCHID);
                    break;
                default:
                    System.out.println("Have you chosen all flowerpots you need? (yes/no)");
                    if(in.getConfirmation()) {
                        cont = false;
                    }
            }
        }
    }

    public void findFlowerpots(List<FlowerGood> fps) {
        for(FlowerStock.FlowerItem f : order) {
            for (FlowerGood fp : fps) {
                if (fp.getName().equals(f)) {
                    flowerpots.add(fp);
                    break;
                }
            }
        }
    }

    private void sortFlowerpots(){
        flowerpots.sort(new FlowerComparator());
    }

    private void printMenu(){
        System.out.println("1 - CACTUS\n2 - ORCHID");
    }
}
