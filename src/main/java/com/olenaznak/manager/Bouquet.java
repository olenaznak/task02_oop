package com.olenaznak.manager;

import com.olenaznak.consoleworker.ConsoleInput;
import com.olenaznak.goods.FlowerGood;
import java.util.*;

public class Bouquet {

    List<BouquetElement> order = new ArrayList<>();
    List<FlowerGood> bouquet = new LinkedList<>();

    public Bouquet() {
    }

    public List<BouquetElement> getOrder() {
        return order;
    }

    public void setOrder(List<BouquetElement> order) {
        this.order = order;
    }

    public List<FlowerGood> getBouquet() {
        if(bouquet == null) {
            return null;
        }
        sortFlowersInBouquet();
        return bouquet;
    }

    public void setBouquet(List<FlowerGood> bouquet) {
        this.bouquet = bouquet;
    }

    public void orderBouquet(){
        ConsoleInput in = new ConsoleInput();
        boolean cont = true;
        printMenu();
        while(cont) {
            System.out.println("Choose flower");
            int i = in.getInt();
            switch (i) {
                case 1:
                    System.out.println("How many?");
                    order.add(new BouquetElement(in.getInt(), FlowerStock.FlowerItem.ROSE));
                    break;
                case 2:
                    System.out.println("How many?");
                    order.add(new BouquetElement(in.getInt(), FlowerStock.FlowerItem.DAISY));
                    break;
                case 3:
                    System.out.println("How many?");
                    order.add(new BouquetElement(in.getInt(), FlowerStock.FlowerItem.LILY));
                    break;
                case 4:
                    System.out.println("How many?");
                    order.add(new BouquetElement(in.getInt(), FlowerStock.FlowerItem.GERBERA));
                    break;
                default:
                     System.out.println("Have you chosen all flowers you need? (yes/no)");
                     if(in.getConfirmation()) {
                         cont = false;
                     }
            }
        }
    }

    public void createBouquet(List<FlowerGood> flowers) {
        for (FlowerGood fg: flowers) {
            for (BouquetElement el : order) {
                if (el.getAmount() != 0 && el.getName().equals(fg.getName())){
                    bouquet.add(fg);
                    el.setAmount(el.getAmount()-1);
                }
            }
        }
    }

    private void sortFlowersInBouquet() {
         bouquet.sort(new FlowerComparator());
    }

    private void printMenu() {
        System.out.println("1 - ROSE\n2 - DAISY\n3 - LILY\n4 - GERBERA");
    }

    public boolean checkBouquet(){
        for (BouquetElement el: order) {
            if (el.getAmount() != 0) {
                return false;
            }
        }
        return true;
    }
}
