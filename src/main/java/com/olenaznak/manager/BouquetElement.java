package com.olenaznak.manager;

public class BouquetElement {
    int amount;
    FlowerStock.FlowerItem name;

    public BouquetElement(int amount, FlowerStock.FlowerItem name) {
        this.amount = amount;
        this.name = name;
    }

    public FlowerStock.FlowerItem getName() {
        return name;
    }

    public void setName(FlowerStock.FlowerItem name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
