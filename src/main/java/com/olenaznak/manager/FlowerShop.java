package com.olenaznak.manager;

import com.olenaznak.consoleworker.ConsoleInput;
import com.olenaznak.consoleworker.ConsoleOutput;

public class FlowerShop {
    public static void main (String args[]) {
        FlowerShop shop = new FlowerShop();
        FlowerStock fs = new FlowerStock();
        ConsoleInput input = new ConsoleInput();
        ConsoleOutput output = new ConsoleOutput();
        System.out.println("Hi, customer!\n " +
                "Would you like to see the list of all goods? (yes/no)");
        if(input.getConfirmation()) {
            output.printList(fs.getAllGoods());
        }
        System.out.println("Would you like to see the list of flowers? (yes/no)");
        if(input.getConfirmation()) {
            output.printList(fs.getAllFlowers());
        }
        System.out.println("Would you like to see the list of flowerpots? (yes/no)");
        if(input.getConfirmation()) {
            output.printList(fs.getAllFlowerpots());
        }
        System.out.println("Would you like to make order? (yes/no)");
        if(input.getConfirmation()) {
            System.out.println("Would you like to buy flowerpot or bouquet? (1/2)");
            if(input.getChoice() == 1) {
                FlowerpotOrder fo = new FlowerpotOrder();
                fo.orderFlowerpot();
                fo.findFlowerpots(fs.getAllFlowerpots());
                System.out.println("Your order:");
                output.printList(fo.getFlowerpots());
            }else {
                boolean check;
                Bouquet b;
                do {
                    b = new Bouquet();
                    b.orderBouquet();
                    b.createBouquet(fs.getAllFlowers());
                    check = b.checkBouquet();
                    if(!check){
                        System.out.println("We can`y make your order. Choose another one.");
                    }
                } while (!check);
                System.out.println("Your bouquet:");
                output.printList(b.getBouquet());
            }
        }
        System.out.println("Bye! See you next time!");
    }

}
