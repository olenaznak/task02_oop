package com.olenaznak.manager;

import com.olenaznak.goods.FlowerGood;

import java.util.Comparator;

public class FlowerComparator implements Comparator<FlowerGood> {
    public int compare(FlowerGood a, FlowerGood b){
        if(a.getPrice() == b.getPrice())
            return 0;
        if (a.getPrice() < b.getPrice())
            return 1;
        return -1;
    }
}
