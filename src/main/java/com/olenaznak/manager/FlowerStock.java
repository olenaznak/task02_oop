package com.olenaznak.manager;

import com.olenaznak.goods.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FlowerStock {
    public enum FlowerItem {
        ROSE,
        DAISY,
        GERBERA,
        LILY,
        CACTUS,
        ORCHID,
    }

    private List<FlowerGood> allGoods = new ArrayList<FlowerGood>();
    private List<FlowerGood> allFlowers = new ArrayList<FlowerGood>();
    private List<FlowerGood> allFlowerpots = new ArrayList<FlowerGood>();

    public FlowerStock() {
        fillAllGoods();
        fillAllFlowers();
        fillAllFlowerpots();
    }

    public List<FlowerGood> getAllGoods() {
        return allGoods;
    }

    public void setAllGoods(List<FlowerGood> allGoods) {
        this.allGoods = allGoods;
    }

    public List<FlowerGood> getAllFlowers() {
        return allFlowers;
    }

    public void setAllFlowers(List<FlowerGood> allFlowers) {
        this.allFlowers = allFlowers;
    }

    public List<FlowerGood> getAllFlowerpots() {
        return allFlowerpots;
    }

    public void setAllFlowerpots(List<FlowerGood> allFlowerpots) {
        this.allFlowerpots = allFlowerpots;
    }

    private void fillAllGoods() {
        allGoods.add(new Daisy(FlowerItem.DAISY, 15));
        allGoods.add(new Rose(FlowerItem.ROSE, 30));
        allGoods.add(new Cactus(FlowerItem.CACTUS, 25));
        allGoods.add(new Rose(FlowerItem.ROSE, 30));
        allGoods.add(new Lily(FlowerItem.LILY, 20));
        allGoods.add(new Orchid(FlowerItem.ORCHID, 100));
        allGoods.add(new Cactus(FlowerItem.CACTUS, 25));
        allGoods.add(new Daisy(FlowerItem.DAISY, 15));
        allGoods.add(new Daisy(FlowerItem.DAISY, 15));
        allGoods.add(new Gerbera(FlowerItem.GERBERA, 10));
        allGoods.add(new Gerbera(FlowerItem.GERBERA, 10));
        allGoods.add(new Daisy(FlowerItem.DAISY, 15));
        allGoods.add(new Lily(FlowerItem.LILY, 20));
        allGoods.add(new Lily(FlowerItem.LILY, 20));
        allGoods.add(new Rose(FlowerItem.ROSE, 30));
        allGoods.add(new Rose(FlowerItem.ROSE, 30));
        allGoods.add(new Orchid(FlowerItem.ORCHID, 100));
        allGoods.add(new Cactus(FlowerItem.CACTUS, 25));
        allGoods.add(new Gerbera(FlowerItem.GERBERA, 10));
        allGoods.add(new Daisy(FlowerItem.DAISY, 15));  // 5 - daisies. 4 - roses, 3 - lilies, 3 - gerberas, 2 - orhidss, 3 - cactuses
    }

    private void fillAllFlowers() {
        allFlowers = allGoods.stream()
                .filter(g -> g instanceof Flower)
                .collect(Collectors.toList());
    }

    private void fillAllFlowerpots() {
        allFlowerpots = allGoods.stream()
                .filter(g -> g instanceof Flowerpot)
                .collect(Collectors.toList());
    }
}
