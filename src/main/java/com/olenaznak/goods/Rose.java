package com.olenaznak.goods;

import com.olenaznak.manager.FlowerStock;

public class Rose extends Flower {
    public Rose(final FlowerStock.FlowerItem name, final double price,
                final String color, final boolean isSmell, final boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Rose(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public Rose() {
    }
}
