package com.olenaznak.goods;

import com.olenaznak.manager.FlowerStock;

public class Cactus extends Flowerpot {
    public Cactus(final FlowerStock.FlowerItem name, final double price, final boolean isBloom) {
        super(name, price, isBloom);
    }

    public Cactus(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public Cactus() {
    }
}
