package com.olenaznak.goods;

import com.olenaznak.goods.Flower;
import com.olenaznak.manager.FlowerStock;

public class Daisy extends Flower {
    public Daisy(final FlowerStock.FlowerItem name, final double price,
                 final String color, final boolean isSmell, final boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Daisy(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public Daisy() {
    }
}
