package com.olenaznak.goods;

import com.olenaznak.manager.FlowerStock;

public class Lily extends Flower {
    public Lily(final FlowerStock.FlowerItem name, final double price,
                final String color, final boolean isSmell, final boolean isThorns) {
        super(name, price, color, isSmell, isThorns);
    }

    public Lily(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public Lily() {
    }
}
