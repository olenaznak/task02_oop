package com.olenaznak.goods;

import com.olenaznak.manager.FlowerStock;

public class Orchid extends Flowerpot {
    public Orchid(final FlowerStock.FlowerItem name,
                  final double price, final boolean isBloom) {
        super(name, price, isBloom);
    }

    public Orchid(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public Orchid() {
    }
}
