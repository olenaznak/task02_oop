package com.olenaznak.goods;

import com.olenaznak.manager.FlowerStock;

public abstract class FlowerGood {
    private FlowerStock.FlowerItem name;
    private double price;

    public FlowerGood(final FlowerStock.FlowerItem name, final double price) {
        this.name = name;
        this.price = price;
    }

    public FlowerGood() {
    }

    public FlowerStock.FlowerItem getName() {
        return name;
    }

    public void setName(FlowerStock.FlowerItem name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
