package com.olenaznak.goods;

import com.olenaznak.manager.FlowerStock;

public abstract class Flower extends FlowerGood {
    private String color;
    private boolean isSmell;
    private boolean isThorns;

    public Flower(final FlowerStock.FlowerItem name, final double price,
                  final String color, final boolean isSmell, final boolean isThorns) {
        super(name, price);
        this.color = color;
        this.isSmell = isSmell;
        this.isThorns = isThorns;
    }

    public Flower() {
    }

    public Flower(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isSmell() {
        return isSmell;
    }

    public void setSmell(boolean smell) {
        isSmell = smell;
    }

    public boolean isThorns() {
        return isThorns;
    }

    public void setThorns(boolean thorns) {
        isThorns = thorns;
    }
}
