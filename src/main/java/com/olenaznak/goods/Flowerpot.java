package com.olenaznak.goods;

import com.olenaznak.goods.FlowerGood;
import com.olenaznak.manager.FlowerStock;

public abstract class Flowerpot extends FlowerGood {
    private boolean isBloom;

    public Flowerpot(final FlowerStock.FlowerItem name,
                     final double price, final boolean isBloom) {
        super(name, price);
        this.isBloom = isBloom;
    }

    public Flowerpot() {
    }

    public Flowerpot(final FlowerStock.FlowerItem name, final double price) {
        super(name, price);
    }

    public boolean isBloom() {
        return isBloom;
    }

    public void setBloom(boolean bloom) {
        isBloom = bloom;
    }
}
