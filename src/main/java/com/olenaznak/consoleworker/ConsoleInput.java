package com.olenaznak.consoleworker;

import java.util.Scanner;

public class ConsoleInput {
    public final int getInt() {
        Scanner in = new Scanner(System.in);
        while (!in.hasNextInt()) {
            System.out.println("Incorrect input, try one more time");
            in.next();
        }
        return in.nextInt();
    }

    public final String getString() {
        String str;
        Scanner in = new Scanner(System.in);
            while (!in.hasNextLine()) {
                System.out.println("Incorrect input, try one more time");
                in.next();
            }
            str = in.nextLine();
        return str;
    }

    public final boolean getConfirmation() {
        while (true) {
            String confirm = getString();
            if (confirm.equals("yes")) {
                return true;
            }
            if (confirm.equals("no")) {
                return false;
            }
            System.out.println("Incorrect input, try one more time");
        }
    }

    public final int getChoice() {
        while (true) {
            int choice = getInt();
            if (choice == 1) {
                return 1;
            }
            if (choice == 2) {
                return 2;
            }
            System.out.println("Incorrect input, try one more time");
        }
    }
}
