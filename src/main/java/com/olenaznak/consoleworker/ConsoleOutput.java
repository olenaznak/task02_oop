package com.olenaznak.consoleworker;

import com.olenaznak.goods.FlowerGood;


import java.util.List;

public class ConsoleOutput {
    public final void printList(final List<FlowerGood> list) {
        list.stream()
                .map(FlowerGood::getName)
                .forEach(System.out::println);
    }

}
